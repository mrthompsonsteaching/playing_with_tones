# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""
This file contains the main functions for generating sounds.  The
content of this file was produced thanks to guidance from the following
references:

* https://shallowsky.com/blog/programming/python-play-chords.html
* https://stackoverflow.com/questions/63980583/how-can-i-play-a-sine-square-wave-using-pygame

"""
import logging
from enum import Enum

import numpy as np
import pygame

from typing import NoReturn
from numpy.typing import ArrayLike

logger = logging.getLogger(__name__)

SAMPLING_FREQUENCY = 44100
BITS_PER_SAMPLE = 32  # unsigned (see init_game)
STEREO_SOUND = 2  # This should not change, this code is for stereo sound


class Channels(Enum):
    LEFT = -1
    BOTH = 0
    RIGHT = 1


def init_pygame() -> NoReturn:
    """
    Initialises the audio device in the PyGame library.  See here:
    https://www.pygame.org/docs/ref/mixer.html#pygame.mixer.init
    """
    pygame.mixer.init(
        frequency=SAMPLING_FREQUENCY,
        size=BITS_PER_SAMPLE,
        channels=STEREO_SOUND,
    )
    logger.debug("Pygame mixer init settings: %s", pygame.mixer.get_init())


def generate_sine_wave(
    tone_frequency: float = 440.0,
    duration: float = 1.0,
    peak_amplitude: float = 1.0,
    phase_shift: float = 0.0,
    channel: Channels = Channels.BOTH,
) -> ArrayLike:
    """
    Important references for this function:

    https://www.pygame.org/docs/ref/sndarray.html
    https://www.pygame.org/docs/ref/mixer.html#pygame.mixer.Sound

    :param tone_frequency:  The sound frequency in Hertz
    :param duration:  The number of seconds (can have decimals) to generate the sound for
    :param peak_amplitude:  Multiply the sound by a value between -1 and 1
    :param phase_shift:  Phase shift the wave by +- 1 wavelength.
    :param channel:  What speaker to play the sound out of
    :return:
    """
    samples_required = int(SAMPLING_FREQUENCY * duration)
    mono_buffer = peak_amplitude * np.sin(
        2 * np.pi * np.arange(samples_required) * tone_frequency / SAMPLING_FREQUENCY
        + phase_shift * 2 * np.pi
    )
    if phase_shift:
        logger.info(
            "Sound wave created as:  y=sin(2*pi*x*%f + %f*2*pi).  One period is %fs.",
            tone_frequency,
            phase_shift,
            1 / tone_frequency,
        )
    else:
        logger.info(
            "Sound wave created as:  y=sin(2*pi*x*%f).   One period is %fs.",
            tone_frequency,
            1 / tone_frequency,
        )
    if channel == Channels.LEFT:
        left_buffer = mono_buffer
        right_buffer = np.zeros(samples_required)
    elif channel == Channels.RIGHT:
        left_buffer = np.zeros(samples_required)
        right_buffer = mono_buffer
    else:
        left_buffer = mono_buffer
        right_buffer = mono_buffer

    buffer = np.zeros(samples_required * 2)
    buffer[0::2] = left_buffer
    buffer[1::2] = right_buffer

    logger.debug("Sound buffer with %i elements created", len(buffer))
    return buffer.astype(np.float32)


def play_sounds(*args: ArrayLike):
    """
    Plays the sounds all passed through as arguments.

    This function assumes that all sounds are formatted the same
    and will mix the sounds together to form a single channel.
    Note that pygame does provide additional mixing tools that might
    bee handy later when trying to adjust system playback response.

    This functional is also limited to having all input arrays of the
    same length.

    Also note that sound is played in threads and as such
    this function waits enough time for the sound to finish playing.
    """
    base_array = np.zeros(len(args[0]))
    for a in args:
        base_array += a

    # Reduce amplitude to avoid clipping
    max_val = max(base_array)
    logger.debug("Max sample is %f", max_val)
    base_array = 1 / max_val * base_array

    s = pygame.mixer.Sound(base_array.astype(np.float32))
    sound_duration = int(s.get_length() * 1000)
    logger.debug("Playing a sound of %i ms length", sound_duration)

    s.play(loops=0)

    pygame.time.wait(sound_duration)


if __name__ == "__main__":

    def main():
        init_pygame()
        duration = 1.0
        play_sounds(
            generate_sine_wave(
                tone_frequency=440, duration=duration, channel=Channels.BOTH
            ),
            generate_sine_wave(
                tone_frequency=240, duration=duration, channel=Channels.BOTH
            ),
        )

    logging.basicConfig(level=logging.DEBUG)
    main()
