# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: MIT

"""
This is intended as the main entrypoint for playing with tones.

Consider some of the following sites to experiment with different
frequencies to use:

* https://frequencygen.com/
* https://onlinetonegenerator.com/

Some additional reading on Chords:

* https://www.libertyparkmusic.com/common-piano-chords-how-to-use/
* https://www.liveabout.com/understanding-dissonant-and-consonant-chords-2456562

"""
import logging
from playing_with_tones import init_pygame, play_sounds, generate_sine_wave, Channels
from music_notes import MusicNotes


def main_consonant():
    init_pygame()
    duration = 1.0

    sine_waves = []

    # The following list of frequencies is the frequency and the volume of that frequency
    for frequency, peak in (
        # fmt: off
        (MusicNotes.C4, 1.0),
        (MusicNotes.E4, 1.0),
        (MusicNotes.G4, 1.0),
        # fmt: on
    ):
        sine_waves.append(
            generate_sine_wave(
                tone_frequency=frequency, duration=duration, peak_amplitude=peak
            )
        )

    play_sounds(*sine_waves)


def main_dissonant():
    init_pygame()
    duration = 1.0

    sine_waves = []

    # The following list of frequencies is the frequency and the volume of that frequency
    for frequency, peak in (
        # fmt: off
        (MusicNotes.C4, 1.0),
        (MusicNotes.D4, 1.0),
        # fmt: on
    ):
        sine_waves.append(
            generate_sine_wave(
                tone_frequency=frequency, duration=duration, peak_amplitude=peak
            )
        )

    play_sounds(*sine_waves)


def main_interference():
    init_pygame()
    duration = 10.0

    sine_waves = []

    # The following list of frequencies is the frequency and the volume of that frequency
    for frequency, peak, phase_shift, ch in (
        # fmt: off
        (MusicNotes.C4, 1.0, 0.0, Channels.LEFT),
        (MusicNotes.C4, 1.0, 0.5, Channels.RIGHT),
        # fmt: on
    ):
        sine_waves.append(
            generate_sine_wave(
                tone_frequency=frequency,
                duration=duration,
                peak_amplitude=peak,
                phase_shift=phase_shift,
                channel=ch,
            )
        )

    play_sounds(*sine_waves)


def main_interesting():
    """
    These are interesting to visualise in Geogebra.  Look at the console output
    for the equivalent sine function that can be copied into Geogebra.  By
    replacing the frequency with a letter and sliding the value you can observe
    interesting interference between different waves.  What do they sound like?
    """
    init_pygame()
    duration = 3.0

    sine_waves = []

    # The following list of frequencies is the frequency and the volume of that frequency
    for frequency, peak in (
        # fmt: off
        (276, 4.0),
        (MusicNotes.C4, 1.0),
        # fmt: on
    ):
        sine_waves.append(
            generate_sine_wave(
                tone_frequency=frequency,
                duration=duration,
                peak_amplitude=peak,
            )
        )

    play_sounds(*sine_waves)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    main_consonant()
    main_dissonant()
    main_interference()
    main_interesting()
